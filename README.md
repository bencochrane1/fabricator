![Alt text](http://fbrctr.github.io/assets/toolkit/images/logo.svg)
# UI styleguide generator for Prismatik

> _fabricate_ - to make by assembling parts or sections.

Fabricator is a tool for building website UI toolkits - _think ["Tiny Bootstraps, for Every Client"](http://daverupert.com/2013/04/responsive-deliverables/#tiny-bootstraps-for-every-client)_

## Quick Start

```shell
$ git clone https://bencochrane1@bitbucket.org/bencochrane1/fabricator.git
$ npm run build
```

We can then build our components using Sass in this file:
```shell
src/assets/toolkit/styles/toolkit.scss
```

To build components, create new html files in this folder:
```shell
src/materials/components/
```

To build structures like forms, create new html files in this folder:
```shell
src/materials/structures/
```

## For each new client...

Create a new cloned repo for each client project. Run 'npm run build' and then drag and drop your dist folder into your bitballoon.com account (free if you don't already have one). You then have a client specific styleguide URL. Woo!

## Documentation

#### [Read the docs →](http://fbrctr.github.io/docs)

## Demo

#### [Default Fabricator Instance →](http://fabricator.bitballoon.com/)
